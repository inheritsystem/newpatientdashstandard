﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using NewPatientDashStandard;
using NewPatientDashStandard.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomProgressBar), typeof(ColorProgressBarRenderer))]
namespace NewPatientDashStandard.Droid
{
   public class ColorProgressBarRenderer: ProgressBarRenderer
    {
        public ColorProgressBarRenderer()
        {

        }
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.ProgressBar> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement == null)
                return;

            if (Control != null)
            {
                UpdateBarColor();
            }
        }
        private void UpdateBarColor()
        {
            var element = Element as CustomProgressBar;
            // http://stackoverflow.com/a/29199280
            Control.IndeterminateDrawable.SetColorFilter(element.BarColor.ToAndroid(), PorterDuff.Mode.SrcIn);
            Control.ProgressDrawable.SetColorFilter(element.BarColor.ToAndroid(), PorterDuff.Mode.SrcIn);
            Control.ScaleY = 6;
            //Control.ScaleX = 70;
          //  Control.
        }
    }
}