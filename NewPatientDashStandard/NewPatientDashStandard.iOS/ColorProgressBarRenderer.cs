﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using CoreGraphics;
using Foundation;
using NewPatientDashStandard;
using NewPatientDashStandard.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomProgressBar), typeof(ColorProgressBarRenderer))]

namespace NewPatientDashStandard.iOS
{
   public class ColorProgressBarRenderer: ProgressBarRenderer
    {
        public ColorProgressBarRenderer()
        {

        }
        protected override void OnElementChanged(ElementChangedEventArgs<ProgressBar> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement==null)
            {
                return;
            }
            if(Control!=null)
            {
                UpdateColor();
            }
        }
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            var x = 1.0f;
            var y = 10.0f;
            CGAffineTransform transform = CGAffineTransform.MakeScale(x, y);
            this.Transform = transform;
            this.Layer.CornerRadius = 10;
        }

        private void UpdateColor()
        {
            var element = Element as CustomProgressBar;
            Control.TintColor = element.BarColor.ToUIColor();
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if(e.PropertyName== CustomProgressBar.BarColorProperty.PropertyName)
            {
                UpdateColor();
            }
        }
    }
}