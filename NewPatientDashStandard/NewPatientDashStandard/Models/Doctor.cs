﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace NewPatientDashStandard.Models
{
  public class Doctor
    {
        public String Name { get; set; }
        public string  Address { get; set; }
        public double Latitude { get; set; }
        public double Logitude { get; set; }
        public string ImageURl { get; set; }
        public string PinValue { get; set; }
    }
}
