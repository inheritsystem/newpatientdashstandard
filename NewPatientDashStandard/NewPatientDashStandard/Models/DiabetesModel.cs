﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace NewPatientDashStandard.Models
{
  public   class DiabetesModel
    {
        public string Name { get; set; }
      
        public int Value { get; set; }
        public double progress { get; set; }
        public Color color { get; set; }

    }
}
