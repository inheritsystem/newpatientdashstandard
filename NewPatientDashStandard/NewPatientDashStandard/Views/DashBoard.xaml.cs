﻿using NewPatientDashStandard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NewPatientDashStandard.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DashBoard : ContentPage
	{
		public DashBoard ()
		{
			InitializeComponent ();
            hemoglobin.Source = ImageSource.FromFile("Tick.png");
            Diabetes.Source = ImageSource.FromFile("Cross.png");
            Flu.Source = ImageSource.FromFile("Cross.png");
            LDL.Source = ImageSource.FromFile("Tick.png");
            Diastolic.Source = ImageSource.FromFile("Tick.png");
            Statin.Source = ImageSource.FromFile("Tick.png");
            Microalbumin.Source = ImageSource.FromFile("Cross.png");
            Dential.Source = ImageSource.FromFile("Cross.png");
            Pneumonia.Source = ImageSource.FromFile("Tick.png");
            Creatinine.Source = ImageSource.FromFile("Tick.png");
            Smoking.Source = ImageSource.FromFile("Tick.pnng");
            Systolic.Source = ImageSource.FromFile("Tick.png");
            Eye.Source = ImageSource.FromFile("Tick.png");
            Foot.Source = ImageSource.FromFile("Tick.png");
            Heart.Source = ImageSource.FromFile("Tick.png");
            Aspirin.Source = ImageSource.FromFile("Tick.png");
            Stroke.Source = ImageSource.FromFile("Tick.png");
            
            List<DiabetesModel> list = new List<DiabetesModel>()
            {
                new DiabetesModel{Name="Healthy Eating",progress=0.3,Value=1,color=Color.Aqua},
                new DiabetesModel{Name="Being Active",progress=0.4,Value=4,color=Color.Blue},
                new DiabetesModel{Name="Monitoring",progress=0.3,Value=5,color=Color.Chocolate},
                new DiabetesModel{Name="Medications",progress=0.6,Value=7,color=Color.BlueViolet},
                new DiabetesModel{Name="Problem solving",progress=0.8,Value=8,color=Color.Red},
                new DiabetesModel{Name="Reducing Risks",progress=0.1,Value=9,color=Color.SandyBrown},
                new DiabetesModel{Name="Healthy",progress=0.9,Value=10,color=Color.Silver}
            };
            DiabetesListView.ItemsSource = list;


            List<DiabetesModel> list1 = new List<DiabetesModel>()
            {
                new DiabetesModel{Name="Carbs",progress=0.3,Value=1,color=Color.Aqua},
                new DiabetesModel{Name="Protein",progress=0.4,Value=3,color=Color.Blue},
                new DiabetesModel{Name="Fat",progress=0.3,Value=4,color=Color.Chocolate},
                new DiabetesModel{Name="Calories",progress=0.6,Value=7,color=Color.BlueViolet},
            };
            CarbsListView.ItemsSource = list1;
        }
        void SelectedItem(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            DiabetesListView.SelectedItem = null;

        }
    }
}