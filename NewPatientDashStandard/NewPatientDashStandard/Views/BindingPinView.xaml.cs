﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NewPatientDashStandard.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BindingPinView : StackLayout
    {
        private string _display;
        private string _name;
        public BindingPinView (string display,string Name)
		{
			InitializeComponent ();
            _display = display;
            _name = Name;
            BindingContext = this;
        }
        public string Display
        {

            get { return _display; }
        }
        public string Name
        {
            get { return _name; }
        }
    }
}