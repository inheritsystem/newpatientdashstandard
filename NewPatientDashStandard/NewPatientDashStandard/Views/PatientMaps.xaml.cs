﻿

using NewPatientDashStandard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace NewPatientDashStandard.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PatientMaps : ContentPage
	{
		public PatientMaps ()
		{
            
			InitializeComponent ();
            List<Pin> pins = new List<Pin>()
            {
                new Pin {Position=new Position(29.746004, -95.491694), Label="Marker",Icon=BitmapDescriptorFactory.FromView(new BindingPinView("85%","dr raj malhotra family practice")),Address="6154 Del Monte Dr Houston, TX 77057 USA"},
                new Pin {Position=new Position(29.818074, -95.434974), Label="Marker",Icon=BitmapDescriptorFactory.FromView(new BindingPinView("2/10","Kaushik")),Address="Oak Forest/ Garden Oaks Houston, TX, USA"},
                new Pin {Position=new Position(29.842071, -95.369699), Label="Marker",Icon=BitmapDescriptorFactory.FromView(new BindingPinView("15%","Pruthvi")),Address="500 Tidwell Rd Houston, TX 77022, USA"},
                new Pin {Position=new Position(29.687375, -95.359337), Label="Marker",Icon=BitmapDescriptorFactory.FromView(new BindingPinView("9/10","Yogesh")),Address="OST/ South Union Houston, TX, USA"}

            };
           // Marker marker = new Marker(Position(29.746004, -95.491694),Title="Marker",Icon=Xamarin.Forms.GoogleMaps.BitmapDescriptorFactory.FromView(new BindingPinView("2/10","Kaushik"))
            foreach (var pin in pins)
            {
                Map.Pins.Add(pin);
               
            }
            Map.PinClicked += Map_PinClicked;
       
            MapDetailsList.IsVisible = false;
        }
        private void Map_PinClicked(object sender, PinClickedEventArgs e)
        {
            var pin = sender as Pin;
            List<Doctor> Doctors = new List<Doctor>()
            {
                new Doctor{Name="Pavan",Address="6154 Del Monte Dr Houston, TX 77057 USA",Latitude=29.746004,Logitude=-95.491694,PinValue="85%"},
                new Doctor{Name="Kaushik",Address="Oak Forest/ Garden Oaks Houston, TX, USA",Latitude=29.818074,Logitude= -95.434974,PinValue="8/10"},
                  new Doctor{Name="Pruthvi",Address="500 Tidwell Rd Houston, TX 77022, USA",Latitude=29.842071,Logitude= -95.369699,PinValue="15%"},
                  new Doctor{Name="Yogesh",Address="OST/ South Union Houston, TX, USA",Latitude=29.687375,Logitude= -95.359337,PinValue="9/10"}
            };
            MapDetailsList.ItemsSource = Doctors;
            MapDetailsList.IsVisible = true;            
        }
        public void Map1_Clicked(object s,EventArgs e)
        {
            Map89.IsVisible = false;
            Map90.IsVisible = true;
        }
        public void Map2_Clicked(object s, EventArgs e)
        {
            Map89.IsVisible = true;
            Map90.IsVisible = false;
        }
        public void Selected_Map_Detail(object s, ItemTappedEventArgs e)
        {
            var mod = e.Item as Doctor;
            var pos = new Position(mod.Latitude, mod.Logitude);
            List<Doctor> Doctors = new List<Doctor>()
            {
                new Doctor{Name="Pavan",Address="6154 Del Monte Dr Houston, TX 77057 USA",Latitude=29.746004,Logitude=-95.491694,PinValue="85%"},
                new Doctor{Name="Kaushik",Address="Oak Forest/ Garden Oaks Houston, TX, USA",Latitude=29.818074,Logitude= -95.434974,PinValue="8/10"},
                  new Doctor{Name="Pruthvi",Address="500 Tidwell Rd Houston, TX 77022, USA",Latitude=29.842071,Logitude= -95.369699,PinValue="15%"},
                  new Doctor{Name="Yogesh",Address="OST/ South Union Houston, TX, USA",Latitude=29.687375,Logitude= -95.359337,PinValue="9/10"}
            };
            MapDetailsList.ItemsSource = Doctors;
            Map.MoveCamera(CameraUpdateFactory.NewCameraPosition(
                    new CameraPosition(
                        pos, // Tokyo
                        17d // zoom
                        )));
        }
    }
	
}